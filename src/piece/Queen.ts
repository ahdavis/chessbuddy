/*
 * Queen.ts
 * Defines a class that represents a chess queen
 * Created on 3/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Piece } from './Piece';
import { Rook } from './Rook';
import { Bishop } from './Bishop';
import { PieceColor } from './PieceColor';
import { PieceType } from './PieceType';
import { Move } from '../board/Move';
import { BoardPos } from '../board/BoardPos';
import { BoardRank } from '../board/BoardRank';
import { BoardFile } from '../board/BoardFile';

/**
 * A chess queen
 */
export class Queen extends Piece {
	//no fields

	/**
	 * Constructs a new Queen instance
	 *
	 * @param newColor The color of the Queen
	 * @param newPos The position of the Queen
	 */
	constructor(newColor: PieceColor, newPos: BoardPos) {
		//call the superclass constructor
		super(PieceType.QUEEN, newColor, newPos, 9);
	}

	/**
	 * Returns an array of all possible
	 * moves for the Queen. See
	 * [[Piece.getPossibleMoves]] for
	 * more information.
	 *
	 * @returns All possible moves for the Queen
	 */
	getPossibleMoves(): Move[] {
		//declare the return array
		let ret = new Array<Move>();

		//get the Queen's diagonal moves
		let bishop = new Bishop(PieceColor.WHITE,
					this._currentPosition);
		let bmoves = bishop.getPossibleMoves();
		for(var i = 0; i < bmoves.length; i++) {
			ret.push(bmoves[i]);
		}

		//get the Queen's orthogonal moves
		let rook = new Rook(PieceColor.WHITE,
					this._currentPosition);
		let rmoves = rook.getPossibleMoves();
		for(var i = 0; i < rmoves.length; i++) {
			ret.push(rmoves[i]);
		}

		//and return the populated array
		return ret;
	}
}

//end of file
