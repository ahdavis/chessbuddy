/*
 * King.ts
 * Defines a class that represents a chess king
 * Created on 3/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Piece } from './Piece';
import { Castler } from './Castler';
import { PieceType } from './PieceType';
import { PieceColor } from './PieceColor';
import { Move } from '../board/Move';
import { BoardPos } from '../board/BoardPos';
import { BoardFile } from '../board/BoardFile';
import { BoardRank } from '../board/BoardRank';

/**
 * A chess king
 */
export class King extends Piece implements Castler {
	//field

	/**
	 * Has the King moved?
	 */
	private _hasMoved: boolean;

	/**
	 * Constructs a new King instance
	 *
	 * @param newColor The color of the King
	 * @param newPos The position of the King
	 */
	constructor(newColor: PieceColor, newPos: BoardPos) {
		//call the superclass constructor
		super(PieceType.KING, newColor, newPos, 
			Number.POSITIVE_INFINITY);

		//and init the field
		this._hasMoved = false;
	}

	/**
	 * Has the King moved, and is therefore unable to castle?
	 *
	 * @returns Whether the King has moved
	 */
	get hasMoved(): boolean {
		return this._hasMoved;
	}

	/**
	 * Moves the King to a new board position.
	 * See [[Piece.moveTo]] for more information.
	 *
	 * @param pos The position to move the King to
	 *
	 * @returns Whether the move was successful
	 */
	moveTo(pos: BoardPos): boolean {
		//attempt to move the King
		let res = super.moveTo(pos);

		//update the moved flag
		if(res) {
			this._hasMoved = true;
		}

		//and return the result
		return res;
	}

	/**
	 * Returns the moves that the King
	 * must make in order to castle.
	 * See [[Castler.getCastleMove]]
	 * for more information.
	 */
	getCastleMove(): Move | Move[] | null {
		//make sure the King hasn't moved
		if(this._hasMoved) {
			return null;
		}

		//otherwise, create a move array
		let ret = new Array<Move>();

		//populate it
		let leftPos = new BoardPos(this._currentPosition.rank, 
						BoardFile.C);
		let leftMove = new Move(this._currentPosition,
					leftPos);
		ret.push(leftMove);
		let rightPos = new BoardPos(this._currentPosition.rank,
						BoardFile.G);
		let rightMove = new Move(this._currentPosition,
						rightPos);
		ret.push(rightMove);

		//and return it
		return ret;
	}

	/**
	 * Returns an array of all possible moves
	 * for the King. See [[Piece.getPossibleMoves]]
	 * for more information.
	 *
	 * @returns All possible moves for the King
	 */
	getPossibleMoves(): Move[] {
		//declare the return array
		let ret = new Array<Move>();

		//populate it

		//add the castle moves
		if(!this._hasMoved) {
			//get the castle moves
			let cmoves = this.getCastleMove() as Move[];

			//loop and put the moves into the return array
			for(var i = 0; i < cmoves.length; i++) {
				ret.push(cmoves[i]);
			}
		}

		//add the normal moves
		if(this._currentPosition.file > BoardFile.A) {
			if(this._currentPosition.rank > 1) {
				let newPos = new BoardPos(
					this._currentPosition.rank - 1,
					this._currentPosition.file - 1);
				let move = new Move(this._currentPosition,
						newPos);
				ret.push(move);
			}

			if(this._currentPosition.rank < 8) {
				let newPos = new BoardPos(
					this._currentPosition.rank + 1,
					this._currentPosition.file - 1);
				let move = new Move(this._currentPosition,
						newPos);
				ret.push(move);
			}

			let newPos = new BoardPos(
					this._currentPosition.rank,
					this._currentPosition.file - 1);
				let move = new Move(this._currentPosition,
						newPos);
				ret.push(move);

		}
		if(this._currentPosition.file < BoardFile.H) {
			if(this._currentPosition.rank > 1) {
				let newPos = new BoardPos(
					this._currentPosition.rank - 1,
					this._currentPosition.file + 1);
				let move = new Move(this._currentPosition,
						newPos);
				ret.push(move);
			}

			if(this._currentPosition.rank < 8) {
				let newPos = new BoardPos(
					this._currentPosition.rank + 1,
					this._currentPosition.file + 1);
				let move = new Move(this._currentPosition,
						newPos);
				ret.push(move);
			}

			let newPos = new BoardPos(
					this._currentPosition.rank,
					this._currentPosition.file + 1);
				let move = new Move(this._currentPosition,
						newPos);
				ret.push(move);

		}
		if(this._currentPosition.rank > 1) {
			let newPos = new BoardPos(
					this._currentPosition.rank - 1,
					this._currentPosition.file);
			let move = new Move(this._currentPosition,
						newPos);
			ret.push(move);
		}
		if(this._currentPosition.rank < 8) {
			let newPos = new BoardPos(
					this._currentPosition.rank + 1,
					this._currentPosition.file);
			let move = new Move(this._currentPosition,
						newPos);
			ret.push(move);
		}

		//and return it
		return ret;
	}
}

//end of file
