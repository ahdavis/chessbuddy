/*
 * Rook.ts
 * Defines a class that represents a chess rook
 * Created on 3/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Piece } from './Piece';
import { PieceType } from './PieceType';
import { PieceColor } from './PieceColor';
import { BoardPos } from '../board/BoardPos';
import { BoardRank } from '../board/BoardRank';
import { BoardFile } from '../board/BoardFile';
import { Castler } from './Castler';
import { Move } from '../board/Move';

/**
 * A chess rook
 */
export class Rook extends Piece implements Castler {
	//field
	/**
	 * Has the Rook moved?
	 */
	private _hasMoved: boolean;

	/**
	 * Constructs a new Rook object
	 *
	 * @param newColor The color of the Rook
	 * @param newPos The position of the Rook
	 */
	constructor(newColor: PieceColor, newPos: BoardPos) {
		//call the superclass constructor
		super(PieceType.ROOK, newColor, newPos, 5);

		//and init the field
		this._hasMoved = false;
	}

	/**
	 * Returns whether the Rook has moved
	 *
	 * @returns whether the Rook has moved
	 */
	get hasMoved(): boolean {
		return this._hasMoved;
	}

	/**
	 * Returns the move the Rook must make
	 * in order to castle
	 *
	 * @return The move the Rook must make in order to castle
	 */
	getCastleMove(): Move | Move[] | null {
		//get whether the Rook is on the A-file
		//or the H-file
		if(this._currentPosition.file === BoardFile.A) {
			//get the position of the castle space
			let newPos = new BoardPos(
				this._currentPosition.rank,
				this._currentPosition.file + 3);

			//and generate the move
			return new Move(this._currentPosition,
					newPos);
		} else if(this._currentPosition.file === BoardFile.H) {
			//get the position of the castle space
			let newPos = new BoardPos(
				this._currentPosition.rank,
				this._currentPosition.file - 2);

			//and generate the move
			return new Move(this._currentPosition,
					newPos);
		} else { //no legal castle
			return null;
		}
	}

	/**
	 * Moves the Rook to a new board position.
	 * See [[Piece.moveTo]] for details
	 *
	 * @param pos The position to move the Rook to
	 *
	 * @returns Whether the move was successful
	 */
	moveTo(pos: BoardPos): boolean {
		//attempt to move the Rook
		let res = super.moveTo(pos);

		//update the hasMoved flag
		if(res) {
			this._hasMoved = true;
		}

		//and return the result
		return res;
	}

	/**
	 * Returns an array of possible moves for the Rook.
	 * See [[Piece.getPossibleMoves]] for more information
	 *
	 * @returns All possible moves for the Rook
	 */
	getPossibleMoves(): Move[] {
		//declare the return array
		let ret = new Array<Move>();
		
		//add castling moves
		if(!this._hasMoved) {
			ret.push(this.getCastleMove() as Move);
		}

		//loop and add all moves in the same file
		//as the Rook
		for(var x = 1; x <= 8; x++) {
			let newPos = new BoardPos(x, 
					this._currentPosition.file);
			let move = new Move(this._currentPosition,
						newPos);
			ret.push(move);
		}

		//loop and add all moves in the same rank as the Rook
		for(var y = BoardFile.A; y <= BoardFile.H; y++) {
			let newPos = new BoardPos(
					this._currentPosition.rank, y);

			let move = new Move(this._currentPosition,
						newPos);

			ret.push(move);
		}

		//and return the populated array
		return ret;
	}
}

//end of file
