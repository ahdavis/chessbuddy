/*
 * Pawn.ts
 * Defines a class that represents a chess pawn
 * Created on 3/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Piece } from './Piece';
import { PieceType } from './PieceType';
import { PieceColor } from './PieceColor';
import { Move } from '../board/Move';
import { BoardPos } from '../board/BoardPos';
import { BoardRank } from '../board/BoardRank';
import { BoardFile } from '../board/BoardFile';

/**
 * A chess pawn
 */
export class Pawn extends Piece {
	//field
	/**
	 * Has the Pawn moved from its starting square?
	 */
	private _hasMoved: boolean;


	/**
	 * Constructs a new Pawn object
	 *
	 * @param newColor The color of the Pawn
	 * @param newPos The position of the Pawn
	 */
	constructor(newColor: PieceColor, newPos: BoardPos) {
		//call the superclass constructor
		super(PieceType.PAWN, newColor, newPos, 1);

		//and init the field
		this._hasMoved = false;
	}	

	/**
	 * Getter method for whether the Pawn has moved
	 *
	 * @returns whether the Pawn has moved
	 */
	get hasMoved(): boolean {
		return this._hasMoved;
	}

	/**
	 * Moves the Pawn to a new position.
	 * See [[Piece.moveTo]] for more 
	 * information.
	 *
	 * @param pos The position to move the Pawn to
	 *
	 * @returns Whether the move was successful
	 */
	moveTo(pos: BoardPos): boolean {
		//call the superclass method
		let res = super.moveTo(pos);

		//update the hasMoved field
		if(res) {
			this._hasMoved = true;
		}

		//and return the result
		return res;
	}

	/**
	 * Returns an array of all possible
	 * moves for the Pawn. See [[Piece.getPossibleMoves]]
	 * for more information.
	 *
	 * @returns All possible moves for the Pawn
	 */
	getPossibleMoves(): Move[] {
		//create the return array
		let ret = new Array<Move>();

		//get the rank of the end of the board
		//based on the Pawn's color
		let endRank = 8; //default to white
		if(this._pieceColor === PieceColor.BLACK) {
			endRank = 1;
		}

		//populate the array 

		//handle the pawn's default move
		if(endRank !== this._currentPosition.rank) {
			if(this._pieceColor === PieceColor.WHITE) {
				//get the board position for the next
				//square forward from the Pawn
				let newPos = new BoardPos(
					this._currentPosition.rank + 1,
					this._currentPosition.file);

				//get a Move from the two positions
				let move = new Move(this._currentPosition,
							newPos);

				//and add it to the array
				ret.push(move);
			} else {
				//get the board position for the
				//next square forward from the Pawn
				let newPos = new BoardPos(
					this._currentPosition.rank - 1,
					this._currentPosition.file);

				//get a Move from the two positions
				let move = new Move(this._currentPosition,
							newPos);

				//and add it to the array
				ret.push(move);
			}
		}

		//handle the pawn's "super" move
		if(!this._hasMoved) {
			if(this._pieceColor === PieceColor.WHITE &&
				this._currentPosition.rank === 2) {
				let newPos = new BoardPos(
					this._currentPosition.rank + 2,
					this._currentPosition.file);

				let move = new Move(this._currentPosition,
							newPos);

				ret.push(move);
			} else if(this._pieceColor === PieceColor.BLACK &&
					this._currentPosition.rank === 7) {
				let newPos = new BoardPos(
					this._currentPosition.rank - 2,
					this._currentPosition.file);

				let move = new Move(this._currentPosition,
							newPos);

				ret.push(move);
			}
		}

		//handle the pawn's left capture move
		if(this._currentPosition.file !== BoardFile.A &&
			this._currentPosition.rank !== endRank) {
			if(this._pieceColor === PieceColor.WHITE) {
				let newPos = new BoardPos(
					this._currentPosition.rank + 1,
					this._currentPosition.file - 1);

				let move = new Move(
					this._currentPosition,
					newPos);

				ret.push(move);
			} else {
				let newPos = new BoardPos(
					this._currentPosition.rank - 1,
					this._currentPosition.file - 1);

				let move = new Move(
					this._currentPosition,
					newPos);

				ret.push(move);
			}
		}

		//handle the pawn's right capture move
		if(this._currentPosition.file !== BoardFile.H &&
			this._currentPosition.rank !== endRank) {
			if(this._pieceColor === PieceColor.WHITE) {
				let newPos = new BoardPos(
					this._currentPosition.rank + 1,
					this._currentPosition.file + 1);

				let move = new Move(
					this._currentPosition,
					newPos);

				ret.push(move);
			} else {
				let newPos = new BoardPos(
					this._currentPosition.rank - 1,
					this._currentPosition.file + 1);

				let move = new Move(
					this._currentPosition,
					newPos);

				ret.push(move);
			}
		}

		//and return the populated array
		return ret;
	}
}

//end of file
