/*
 * Castler.ts
 * Defines an interface to be implemented by castleable Pieces
 * Created on 3/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { Move } from '../board/Move';

/**
 * Describes a Piece that can be used
 * to castle, i.e. a Rook or King
 */
export interface Castler {
	/**
	 * Has the Piece moved, and is therefore
	 * unable to castle?
	 */
	hasMoved: boolean;

	/**
	 * Returns the move(s)
	 * that the Piece must make in 
	 * order to castle
	 *
	 * @returns The move(s) that the Piece must make to castle
	 */
	getCastleMove(): Move | Move[] | null;
}

//end of file
