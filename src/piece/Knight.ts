/*
 * Knight.ts
 * Defines a class that represents a chess knight
 * Created on 3/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Piece } from './Piece';
import { PieceType } from './PieceType';
import { PieceColor } from './PieceColor';
import { Move } from '../board/Move';
import { BoardPos } from '../board/BoardPos';
import { BoardRank } from '../board/BoardRank';
import { BoardFile } from '../board/BoardFile';

/**
 * A chess knight
 */
export class Knight extends Piece {
	//no fields

	/**
	 * Constructs a new Knight instance
	 *
	 * @param newColor The color of the Knight
	 * @param newPos The board position of the Knight
	 */
	constructor(newColor: PieceColor, newPos: BoardPos) {
		//call the superclass constructor
		super(PieceType.KNIGHT, newColor, newPos, 3);
	}
	

	/**
	 * Returns all possible moves for the Knight.
	 * See [[Piece.getPossibleMoves]] for more info.
	 *
	 * @returns All possible moves for the Knight
	 */
	getPossibleMoves(): Move[] {
		//create the return array
		let ret = new Array<Move>();
		
		//populate the array

		//handle down-left and down-right moves
		if(this._currentPosition.rank >= 3) {
			if(this._currentPosition.file > BoardFile.A) {
				let newPos = new BoardPos(
					this._currentPosition.rank - 2,
					this._currentPosition.file - 1);

				let move = new Move(this._currentPosition,
					newPos);

				ret.push(move);
			}

			if(this._currentPosition.file < BoardFile.H) {
				let newPos = new BoardPos(
					this._currentPosition.rank - 2,
					this._currentPosition.file + 1);

				let move = new Move(this._currentPosition,
							newPos);

				ret.push(move);
			}
		}

		//handle right-down and right-up moves
		if(this._currentPosition.file <= BoardFile.F) {
			if(this._currentPosition.rank > 1) {
				let newPos = new BoardPos(
					this._currentPosition.rank - 1,
					this._currentPosition.file + 2);

				let move = new Move(this._currentPosition,
							newPos);

				ret.push(move);
			}

			if(this._currentPosition.rank < 8) {
				let newPos = new BoardPos(
					this._currentPosition.rank + 1,
					this._currentPosition.file + 2);

				let move = new Move(this._currentPosition,
							newPos);

				ret.push(move);
			}
		}

		//handle up-left and up-right moves
		if(this._currentPosition.rank <= 6) {
			if(this._currentPosition.file > BoardFile.A) {
				let newPos = new BoardPos(
					this._currentPosition.rank + 2,
					this._currentPosition.file - 1);

				let move = new Move(this._currentPosition,
					newPos);

				ret.push(move);
			}

			if(this._currentPosition.file < BoardFile.H) {
				let newPos = new BoardPos(
					this._currentPosition.rank + 2,
					this._currentPosition.file + 1);

				let move = new Move(this._currentPosition,
							newPos);

				ret.push(move);
			}
		}

		//handle left-down and left-up moves
		if(this._currentPosition.file >= BoardFile.C) {
			if(this._currentPosition.rank > 1) {
				let newPos = new BoardPos(
					this._currentPosition.rank - 1,
					this._currentPosition.file - 2);

				let move = new Move(this._currentPosition,
							newPos);

				ret.push(move);
			}

			if(this._currentPosition.rank < 8) {
				let newPos = new BoardPos(
					this._currentPosition.rank + 1,
					this._currentPosition.file - 2);

				let move = new Move(this._currentPosition,
							newPos);

				ret.push(move);
			}
		}


		//and return the populated array
		return ret;
	}
}

//end of file
