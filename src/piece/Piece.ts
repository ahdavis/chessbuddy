/*
 * Piece.ts
 * Defines a class that represents a chess piece
 * Created on 3/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { PieceType } from './PieceType';
import { PieceColor } from './PieceColor';
import { BoardPos } from '../board/BoardPos';
import { Move } from '../board/Move';
import * as deepEqual from 'deep-equal';

/**
 * A chess piece
 */
export abstract class Piece {
	//fields
	/**
	 * The type of the Piece
	 */
	protected _pieceType: PieceType;

	/**
	 * The color of the Piece
	 */
	protected _pieceColor: PieceColor;

	/**
	 * The current position of the Piece
	 */
	protected _currentPosition: BoardPos;

	/**
	 * The trade value of the Piece
	 */
	private _value: number;

	//method definitions

	/**
	 * Constructs a new Piece object
	 *
	 * @param newType The type of the Piece
	 * @param newColor The color of the Piece
	 * @param newPos The position of the Piece
	 * @param newValue The trade value of the Piece
	 */
	constructor(newType: PieceType, newColor: PieceColor,
			newPos: BoardPos, newValue: number) {
		this._pieceType = newType;
		this._pieceColor = newColor;
		this._currentPosition = newPos;
		this._value = newValue;
	}

	/**
	 * Getter method for the pieceType field
	 *
	 * @returns The type of the Piece
	 */
	get pieceType(): PieceType {
		return this._pieceType;
	}

	/**
	 * Getter method for the pieceColor field
	 *
	 * @returns The color of the Piece
	 */
	get pieceColor(): PieceColor {
		return this._pieceColor;
	}

	/**
	 * Getter method for the currentPosition field
	 *
	 * @returns The current position of the Piece
	 */
	get currentPosition(): BoardPos {
		return this._currentPosition;
	}

	/**
	 * Getter method for the value field
	 *
	 * @returns The trade value of the Piece
	 */
	get value(): number {
		return this._value;
	}

	/**
	 * Moves the Piece to a given board position
	 * after validating that that position is
	 * included in the possible moves from
	 * the Piece's current position.
	 *
	 * @param pos The position to move the Piece to
	 *
	 * @returns Whether the move was successful
	 */
	moveTo(pos: BoardPos): boolean {
		//get the possible moves for the Piece
		let possMoves = this.getPossibleMoves();

		//verify that the argument is in the
		//set of possible moves
		for(var i = 0; i < possMoves.length; i++) {
			//get the current move
			var curMove = possMoves[i];

			//compare its start and end positions with
			//the current board position
			if(deepEqual(curMove.end, pos) &&
				deepEqual(curMove.start, 
						this._currentPosition)) {
				//the move matches, so update the
				//current position
				this._currentPosition = curMove.end;

				//and return a success
				return true;
			}
		}

		//if control reaches here, then the attempted position
		//is not a valid move, so return with a failure
		return false;
	}

	/**
	 * Returns an array containing all possible moves
	 * the Piece can make. Note that this array
	 * is not restricted to only legal moves.
	 *
	 * @returns The possible moves for the Piece
	 */
	abstract getPossibleMoves(): Move[];
}

//end of file
