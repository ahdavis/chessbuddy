/*
 * Bishop.ts
 * Defines a class that represents a chess bishop
 * Created on 3/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Piece } from './Piece';
import { PieceColor } from './PieceColor';
import { PieceType } from './PieceType';
import { Move } from '../board/Move';
import { BoardPos } from '../board/BoardPos';
import { BoardRank } from '../board/BoardRank';
import { BoardFile } from '../board/BoardFile';

/**
 * A chess bishop
 */
export class Bishop extends Piece {
	//no fields

	/**
	 * Constructs a new Bishop instance
	 *
	 * @param newColor The color of the Bishop
	 * @param newPos The position of the Bishop
	 */
	constructor(newColor: PieceColor, newPos: BoardPos) {
		//call the superclass constructor
		super(PieceType.BISHOP, newColor, newPos, 3);
	}


	/**
	 * Returns an array of all possible moves
	 * for the Bishop. See [[Piece.getPossibleMoves]]
	 * for more information.
	 *
	 * @returns All possible moves for the Bishop
	 */
	getPossibleMoves(): Move[] {
		//create the return array
		let ret = new Array<Move>();

		//populate the return array

		//save the current rank and file
		let curRank = this._currentPosition.rank;
		let curFile = this._currentPosition.file;

		//get the moves that are up-left from the Bishop
		while(curRank <= 8 && curFile >= BoardFile.A) {
			let newPos = new BoardPos(curRank, curFile);

			let move = new Move(this._currentPosition,
						newPos);

			ret.push(move);

			curRank++;
			curFile--;
		}

		//refresh the current rank and file
		curRank = this._currentPosition.rank;
		curFile = this._currentPosition.file;

		//get the moves that are up-right of the Bishop
		while(curRank <= 8 && curFile <= BoardFile.H) {
			let newPos = new BoardPos(curRank, curFile);

			let move = new Move(this._currentPosition,
						newPos);

			ret.push(move);

			curRank++;
			curFile++;
		}

		//refresh the current rank and file
		curRank = this._currentPosition.rank;
		curFile = this._currentPosition.file;

		//get the moves that are down-left from the Bishop
		while(curRank >= 1 && curFile >= BoardFile.A) {
			let newPos = new BoardPos(curRank, curFile);

			let move = new Move(this._currentPosition,
						newPos);

			ret.push(move);

			curRank--;
			curFile--;
		}

		//refresh the current rank and file
		curRank = this._currentPosition.rank;
		curFile = this._currentPosition.file;

		//get the moves that are down-right of the Bishop
		while(curRank >= 1 && curFile <= BoardFile.H) {
			let newPos = new BoardPos(curRank, curFile);

			let move = new Move(this._currentPosition,
						newPos);

			ret.push(move);

			curRank--;
			curFile++;
		}

		//and return the populated array
		return ret;
	}
}

//end of file
