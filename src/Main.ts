/*
 * Main.ts
 * Main class for ChessBuddy
 * Created on 3/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { BrowserWindow } from 'electron';

/**
 * Main class for ChessBuddy
 */
export default class Main {
	//fields
	/**
	 * The app window
	 */
	static mainWindow: Electron.BrowserWindow;

	/**
	 * The app itself
	 */
	static application: Electron.App; //the app itself

	/**
	 * A local type for the app window
	 */
	static BrowserWindow;

	//method definitions
	
	/**
	 * Main entry point for the app
	 *
	 * @param app The Electron app object for the class
	 * @param browserWindow The window type to create
	 */
	static main(app: Electron.App, 
			browserWindow: typeof BrowserWindow) {
		//initialize the fields
		Main.BrowserWindow = browserWindow;
		Main.application = app;

		//and set up event handlers
		Main.application.on('window-all-closed', 
					Main.onWindowAllClosed);
		Main.application.on('ready', Main.onReady);
	}

	/**
	 * Called when all app windows are closed
	 */
	private static onWindowAllClosed() {
		//only quit the app if not on macOS
		if(process.platform !== 'darwin') {
			Main.application.quit();
		}
	}

	/**
	 * Called when the app is quit
	 */
	private static onClose() {
		//Dereference the window object
		Main.mainWindow = null;
	}

	/**
	 * Called when the app is launched
	 */
	private static onReady() {
		//create the window
		Main.mainWindow = new Main.BrowserWindow({ 
			width: 800,
			height: 600,
			webPreferences: {
				nodeIntegration: true
			}
		});

		//load the HTML for the window
		Main.mainWindow.loadFile('index.html');

		//set the window's close event handler
		Main.mainWindow.on('closed', Main.onClose);
	}
}

//end of class
