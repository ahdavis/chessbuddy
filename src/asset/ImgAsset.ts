/*
 * ImgAsset.ts
 * Defines a class that represents an image asset
 * Created on 3/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Asset } from './Asset';
import { AssetType } from './AssetType';
import * as path from 'path';

/**
 * An image asset usable by ChessBuddy
 */
export class ImgAsset extends Asset {
	//fields

	/**
	 * The width of the image
	 */
	private _width: number;

	/**
	 * The height of the image
	 */
	private _height: number;

	//methods

	/**
	 * Constructs a new ImgAsset instance
	 *
	 * @param filename The filename of the image
	 * @param path The path to the image's containing directory
	 * @param width The width of the image
	 * @param height The height of the image
	 */
	constructor(filename: string, path: string, 
			width: number, height: number) {
		//call the superclass constructor
		super(filename, AssetType.IMG, path);	

		//and init the fields
		this._width = width;
		this._height = height;
	}

	/**
	 * Getter method for the width field
	 *
	 * @returns The width of the image
	 */
	get width(): number {
		return this._width;
	}

	/**
	 * Getter method for the height field
	 *
	 * @returns The height of the image
	 */
	get height(): number {
		return this._height;
	}

	/**
	 * Returns the HTML code needed 
	 * to embed the image.
	 *
	 * @returns The HTML for the image
	 */
	encode(): string {
		//declare the return value
		let ret = '';

		//populate it 
		ret += '<img width="';
		ret += this._width.toString();
		ret += '" height="';
		ret += this._height.toString();
		ret += '" src="';
		ret += path.join(this._path, this._filename);
		ret += '">';

		//and return it
		return ret;
	}
}

//end of file
