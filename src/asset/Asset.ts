/*
 * Asset.ts
 * Defines a class that represents a resource asset
 * Created on 3/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { AssetType } from './AssetType';

/**
 * A resource asset usable by ChessBuddy
 */
export abstract class Asset {
	//fields

	/**
	 * The filename of the Asset
	 */
	protected _filename: string;

	/**
	 * The type of the Asset
	 */
	protected _assetType: AssetType;

	/**
	 * The path to the Asset's containing directory
	 */
	protected _path: string;

	//methods

	/**
	 * Constructs a new Asset object
	 *
	 * @param filename The filename of the Asset
	 * @param assetType The type of the Asset
	 * @param path The path to the Asset's containing directory
	 */
	constructor(filename: string, assetType: AssetType,
			path: string) {
		this._filename = filename;
		this._assetType = assetType;
		this._path = path;
	}

	/**
	 * Getter method for the filename
	 *
	 * @returns The filename of the Asset
	 */
	get filename(): string {
		return this._filename;
	}

	/**
	 * Getter method for the asset type
	 *
	 * @returns The type of the Asset
	 */
	get assetType(): AssetType {
		return this._assetType;
	}

	/**
	 * Getter method for the path
	 *
	 * @returns The path to the Asset's containing directory
	 */
	get path(): string {
		return this._path;
	}

	/**
	 * Returns the code needed to use the Asset
	 *
	 * @returns The code needed to use the Asset
	 */
	abstract encode(): string;
}

//end of file
