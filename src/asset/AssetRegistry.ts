/*
 * AssetRegistry.ts
 * Defines a registry of different assets
 * Created on 3/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Asset } from './Asset';
import { AssetType } from './AssetType';
import { TSMap } from 'typescript-map';

/**
 * A registry of different assets available to ChessBuddy
 */
export class AssetRegistry {
	//fields

	/**
	 * An instance of the registry
	 */
	private static _instance: AssetRegistry = null;

	/**
	 * The dictionary that stores assets
	 */
	private _dict: TSMap<number, Asset>;

	//methods

	/**
	 * Constructs a new instance of the registry
	 */
	private constructor() {
		//init the field
		this._dict = new TSMap();
	}

	/**
	 * Returns an instance of the registry
	 * and creates one if none exist
	 *
	 * @returns An instance of the registry
	 */
	static get instance(): AssetRegistry {
		//create an instance of the registry
		if(!AssetRegistry._instance) {
			AssetRegistry._instance = new AssetRegistry();
		}

		//and return it
		return AssetRegistry._instance;
	}

	/**
	 * Registers an asset under an ID
	 *
	 * @param id A numeric ID to register the asset under
	 * @param asset The Asset to register
	 */
	register(id: number, asset: Asset) {
		//put the asset into the dictionary
		this._dict.set(id, asset);
	}

	/**
	 * Returns an asset given its ID
	 *
	 * @param id The ID of the desired Asset
	 *
	 * @returns The Asset corresponding to the ID
	 */
	assetForID(id: number): Asset {
		return this._dict.get(id);
	}

	/**
	 * Returns all assets with a given type
	 *
	 * @param assetType The type of asset to search for
	 *
	 * @returns An array containing all assets with a given type
	 */
	assetsWithType(assetType: AssetType): Asset[] {
		//create the return array
		let ret = new Array<Asset>();

		//get all assets in the registry
		//with a given type and add them to
		//the return array
		this._dict.map(function(asset) {
			if(asset.assetType === assetType) {
				ret.push(asset);
			}
		});

		//and return the resulting array
		return ret;
	}
}

//end of file
