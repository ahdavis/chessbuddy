/*
 * SndAsset.ts
 * Defines a class that represents a sound asset
 * Created on 3/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Asset } from './Asset';
import { AssetType } from './AssetType';
import * as path from 'path';

/**
 * A playable sound asset
 */
export class SndAsset extends Asset {
	//no fields

	/**
	 * Constructs a new SndAsset instance
	 *
	 * @param filename The filename of the sound file
	 * @param path The path to the sound file's containing directory
	 */
	constructor(filename: string, path: string) {
		//call the superclass constructor
		super(filename, AssetType.SND, path);
	}
	
	/**
	 * Returns the TypeScript code for playing the sound
	 *
	 * @returns The TypeScript code for playing the sound
	 */
	encode(): string {
		//generate the code
		let code: string = `({
			play: (): boolean => {
				var player = require('play-sound')
						(opts = {});
				player.play(` + path.join(this._path,
							this._filename) + `
						, function(err) {
							if(err) {
							return false;
							} else {
							return true;
						});
			}
		})`;

		//and return it
		return code;
	} 
}

//end of file
