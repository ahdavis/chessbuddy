/*
 * Move.ts
 * Defines a class that represents a chess move
 * Created on 3/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { BoardPos } from './BoardPos';

/**
 * A chess move, defined by a start position
 * and an end position
 */
export class Move {
	//fields
	/**
	 * The start of the move
	 */
	private _start: BoardPos;

	/**
	 * The end of the move
	 */
	private _end: BoardPos;

	//method definitions

	/**
	 * Constructs a new Move object
	 *
	 * @param newStart The starting position of the move
	 * @param newEnd The ending position of the move
	 */
	constructor(newStart: BoardPos, newEnd: BoardPos) {
		this._start = newStart;
		this._end = newEnd;
	}

	/**
	 * Getter method for the start field
	 *
	 * @returns The starting position of the Move
	 */
	get start(): BoardPos {
		return this._start;
	}

	/**
	 * Getter method for the end field
	 *
	 * @returns The ending position of the Move
	 */
	get end(): BoardPos {
		return this._end;
	}
}

//end of file
