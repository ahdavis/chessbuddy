/*
 * BoardSquare.ts
 * Defines a class that represents a chessboard square
 * Created on 3/4/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Optional } from 'typescript-optional';
import { SquareColor } from './SquareColor';
import { BoardPos } from './BoardPos';
import { Piece } from '../piece/Piece';

/**
 * A chessboard square
 */
export class BoardSquare {
	//fields

	/**
	 * The position of the BoardSquare
	 */
	private _position: BoardPos;

	/**
	 * The Piece currently residing
	 * on the BoardSquare. 
	 */
	private _currentPiece: Optional<Piece>;

	/**
	 * The color of the BoardSquare
	 */
	private _color: SquareColor;

	//methods

	/**
	 * Constructs a new BoardSquare instance
	 *
	 * @param newPos The position of the new BoardSquare
	 * @param newColor The color of the new BoardSquare
	 */
	constructor(newPos: BoardPos, newColor: SquareColor) {
		//init the fields
		this._position = newPos;
		this._currentPiece = Optional.empty();
		this._color = newColor;
	}

	/**
	 * Getter method for the position field
	 *
	 * @returns The position of the BoardSquare
	 */
	get position(): BoardPos {
		return this._position;
	}

	/**
	 * Getter method for the current piece
	 * Returns null if no Piece exists on the BoardSquare
	 *
	 * @returns The current Piece on the BoardSquare
	 */
	get currentPiece(): Piece {
		return this._currentPiece.orNull();
	}

	/**
	 * Getter method for the color
	 *
	 * @returns The color of the BoardSquare
	 */
	get color(): SquareColor {
		return this._color;
	}

	/**
	 * Setter method for the current piece
	 *
	 * @param piece The Piece to place on the BoardSquare
	 */
	set currentPiece(piece: Piece) {
		this._currentPiece = Optional.ofNullable(piece);
	 }

	/**
	 * Returns whether the BoardSquare has a Piece on it
	 *
	 * @returns Whether the BoardSquare has a Piece on it
	 */
	hasPiece(): boolean {
		return this._currentPiece.isPresent();
	}

	/**
	 * Removes the current Piece from the BoardSquare
	 *
	 * @returns Whether a Piece was actually removed
	 */
	clearPiece(): boolean {
		//handle no Piece on the square
		if(!this.hasPiece()) {
			return false;
		} else {
			//clear the piece
			this._currentPiece = Optional.empty();

			//and return true
			return true;
		}
	}
}

//end of file
