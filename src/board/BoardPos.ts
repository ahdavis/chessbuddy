/*
 * BoardPos.ts
 * Defines a class that represents a board position
 * Created on 3/3/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { BoardRank } from './BoardRank';
import { BoardFile } from './BoardFile';

/**
 * A position on a chessboard, defined by
 * a rank (y-coord) and file (x-coord)
 */
export class BoardPos {
	//fields
	/**
	 * The rank of the position
	 */
	private _rank: BoardRank;

	/**
	 * The file of the position
	 */
	private _file: BoardFile;

	//method definitions

	/**
	 * Constructs a new BoardPos object
	 *
	 * @param newRank The rank of the position
	 * @param newFile The file of the position
	 */
	constructor(newRank: BoardRank, newFile: BoardFile) {
		//init the fields
		this.rank = newRank;
		this.file = newFile;
	}

	/**
	 * Getter method for the rank field
	 *
	 * @returns The rank of the position
	 */
	get rank(): BoardRank {
		return this._rank;
	}

	/**
	 * Getter method for the file field
	 *
	 * @returns The file of the position
	 */
	get file(): BoardFile {
		return this._file;
	}

	/**
	 * Setter method for the rank field
	 *
	 * @param newRank the new rank of the position
	 */
	set rank(newRank: BoardRank) {
		//validate the new rank
		if(newRank < 1) {
			this._rank = 1;
		} else if(newRank > 8) {
			this._rank = 8;
		} else {
			this._rank = newRank;
		}
	}

	/**
	 * Setter method for the file field
	 *
	 * @param newFile the new file of the position
	 */
	set file(newFile: BoardFile) {
		this._file = newFile;
	}
}

//end of file
